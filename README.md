# Extreme-scale modeling of seismic hazards 

**PD leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/LMU.png?inline=false" width="50">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/IMO.png?inline=false" width="50">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/TUM.png?inline=false" width="50">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/CNRS.png?inline=false" width="50">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/bsc.png?inline=false" width="100">

**Codes:** [SeisSol](https://gitlab.com/cheese5311126/codes/seissol), [ExaHyPE](https://gitlab.com/cheese5311126/codes/exahype), [SPECFEM3D](https://gitlab.com/cheese5311126/codes/specfem3d), [Tandem](https://gitlab.com/cheese5311126/codes/tandem) 

**Derived Simulation Cases:** [SC1.1](https://gitlab.com/cheese5311126/simulation-cases/sc1.1), [SC1.2](https://gitlab.com/cheese5311126/simulation-cases/sc1.2), [SC1.3](https://gitlab.com/cheese5311126/simulation-cases/sc1.3)

## Description

This PD combines high-fidelity earthquake and seismic wave simulations performed with 4
ChEESE-2P flagship codes in order to catapult seismic simulations to bridge spatial and temporal scales by enabling the
efficiency, versatility and flexibility required to allow the integration of multiphysics simulations, physics-based probabilistic
hazard assessment, rapid response urgent earthquake and shaking simulations and modeling the highly complex seismic
wavefield at high-frequencies, such as ubiquitous anthropogenic “noise”. PD1 will allow combining simulations that are well
validated with available data with probabilistic frameworks and uncertainty quantification towards next generation
physics-based hazard assessment. Using a physics-based description of earthquakes, modern numerical methods as well as
hardware-specific optimisation will shed light on the dynamics, severity and cascading hazards of earthquake behavior and
enable an unparalleled degree of realism exploiting high-performance computing. As a complement to forward HPC
modeling, PD1 will also explore ML-based solutions that can obtain fast analogs to seismic simulations, fundamental for
parameter exploration and early assessment of hazard.

## Objective

The objective of this PD is to improve the scalability and time-to-solution of the earthquake modeling and seismic
wave propagation codes SeisSol, ExaHyPE, SPECFEM3D and Tandem. These flagship codes will be enhanced with respect
to new features in model physics (such as viscoelasticity), IO including capabilities for importing and exporting 3D volume
data for one-way or two-way multiphysics coupling, and mesh generation. The developed multiphysics, probabilistic and
data-integration capabilities will be tailored towards future DestinE integration in digital twin components, such as those
developed in DT-GEO, and specific models and data will be prepared to be shared via EPOS, utilizing synergies with
Geo-INQUIRE, and inform rapid response simulations.